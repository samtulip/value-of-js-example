var gulp = require('gulp');
var run = require('gulp-run');
var watch = require('gulp-watch');
var logger = require('gulp-logger');

gulp.task('watch', function() {
    return watch('index.js', function() {
      try {
        run('npm test').exec();
      } catch (error ) {
          logger.log("Script exited with error " + error)
      }
    });
})
