# Experiment with Javascript valueOf

Project created to play with the valueOf function in javascript. This project has no other uses.

## Getting started

```
npm install
npm test
```

## Advanced usage

`gulp watch` - Runs npm test when the index.js file is saved for quick feedback
