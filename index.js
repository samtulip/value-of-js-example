const a = {
  value: 0,
  valueOf: function( ) {
    return ++ this.value;
  }
}

console.log("Check how valueOf works with logic");
console.log("a == 1", a == 1) ; //true
console.log("a == 1 (again)", a == 1) ; //false
console.log("a == 3", a == 3) ; //true
console.log("a === 4", a === 4) ; //false

const b = {
  value: 10,
  valueOf: function( ) {
    return this.value;
  }
}

console.log("Check if valueOf is called when doing simple addition");
console.log( b + 10); // returns 20
console.log( b ); // returns 10
console.log( b + "myString" ); // returns 10myString

const c = {
  bool: true,
  switchOff: function() {
    this.bool = false;
  },
  valueOf: function() {
    return this.bool;
  }
}

console.log("Starting Loop");
var index = 0;
while( c == true ){
  index ++;
  if( index == 10 ){
    c.switchOff();
  }
  console.log("Looping " + index);
}
